from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Prueba1.settings')
app = Celery('tasks', backend='amqp', broker='amqp://Server:123456@192.168.1.76/Server-PC')
#app = Celery('tasks', backend='amqp', broker='amqp://Server:123456@192.168.1.3/Server-PC')


app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)